using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Net.Wifi;
using Android.Util;

namespace ClearWifi_v3
{
	[Activity (Label = "Clean Wifi List", MainLauncher = true)]
	public class MainActivity : ListActivity
	{
		List<string> WifiAccessPointList;
		string selectedSSID;
		string NotifyMessage;
		string NotifyTitle;
		string ConfirmTitle;
		string ConfirmMessage;

		enum DialogType
		{
			Notify = 0,
			Confirm = 1
		}

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			WifiAccessPointList = new List<string> ();
			WifiManager wm = (WifiManager)GetSystemService (Context.WifiService);
			WifiAccessPointList = wm.ConfiguredNetworks.Select (x => x.Ssid).ToList ();
			this.ListAdapter = new ArrayAdapter<String> (this, Android.Resource.Layout.SimpleListItemMultipleChoice, WifiAccessPointList);

			SetContentView (Resource.Layout.Main);

			var ScanBtn = FindViewById<Button> (Resource.Id.scanBtn);
			ScanBtn.Click += (x,y) => {
				LoadWifiList ();
			};

		}

		protected override void OnListItemClick (ListView l, View v, int position, long id)
		{
			this.selectedSSID = (string)l.Adapter.GetItem (position);
			Log.Debug ("Clean Wifi", "you selected " + this.selectedSSID);

			this.ConfirmTitle = "Remove SSID " + this.selectedSSID + "?";
			this.ConfirmMessage = "Are you sure you want to remove this SSID?";

			ShowDialog (Convert.ToInt32 (DialogType.Confirm));
		}

		public void RemoveNetworkConnection ()
		{
			WifiManager wm = (WifiManager)GetSystemService (Context.WifiService);
			int networkId = wm.ConfiguredNetworks.Where (con => con.Ssid == this.selectedSSID).Select (x => x.NetworkId).First ();
			wm.RemoveNetwork (networkId);
			LoadWifiList ();
		}

		public void LoadWifiList ()
		{
			WifiAccessPointList.Clear ();
			WifiManager wm = (WifiManager)GetSystemService (Context.WifiService);
			WifiAccessPointList = wm.ConfiguredNetworks.Select (x => x.Ssid).ToList ();
			ListAdapter = new ArrayAdapter<String> (this, Android.Resource.Layout.SimpleListItem1, WifiAccessPointList);
		}

		protected override Dialog OnCreateDialog (int id)
		{
			var builder = new AlertDialog.Builder (this);
			switch (id) {
			case 1: 
				builder.SetMessage (ConfirmMessage);
				builder.SetTitle (ConfirmTitle);
				builder.SetPositiveButton ("Ok", (s,e) => {
					RemoveNetworkConnection ();
					this.NotifyTitle = "Removed";
					this.NotifyMessage = "The SSID " + this.selectedSSID + " Has been removed successfully.";
					ShowDialog (Convert.ToInt32 (DialogType.Notify));
				});
				builder.SetNegativeButton ("Cancel", (s,e) => {

				});
				return builder.Create ();
			case 0: 
				builder.SetTitle (NotifyTitle);	
				builder.SetMessage (NotifyMessage);
				return builder.Create ();
			}
			return null;
		}
	}
}


